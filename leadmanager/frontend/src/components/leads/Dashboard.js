import React,{Fragment} from 'react';
import Leads from './Leads.js';
import Form from './Form.js';

export default function Dashboard(){
   return(
       <Fragment>
            <Form/>
            <Leads/>
       </Fragment>
   ) 
}

